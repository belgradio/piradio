## Pi radio makefile

SRVDIR   := /srv/radio
SBINDIR  := /usr/local/sbin
BINDIR   := /usr/local/bin
WEBDIR   := /var/www/radio
CGIDIR   := /usr/local/lib/cgi-bin
ETCDIR   := /etc/radio

# the user you run it with has to have access to Pi's audio system
RADIOUSR := pi
RADIOGRP := audio

INSTALLCMD := sudo install -o $(RADIOUSR) -g $(RADIOGRP)

install:
#	radio daemon files
	$(INSTALLCMD) -m 0755 -d $(SRVDIR)
	$(INSTALLCMD) -m 0755 radiocmd $(BINDIR)
	$(INSTALLCMD) -m 0755 radiosrv $(SBINDIR)
	$(INSTALLCMD) -m 0755 -d $(ETCDIR)
	$(INSTALLCMD) -m 0644 omx.keys $(ETCDIR)
	$(INSTALLCMD) -m 0644 radio /etc/default

#	systemd service 
	$(INSTALLCMD) -m 0644 radio.service /etc/systemd/system
	sudo systemctl enable radio.service
	sudo systemctl start radio.service

#	web part, it works with any http server supporting CGI
	$(INSTALLCMD) -m 0755 -d $(WEBDIR) 
	$(INSTALLCMD) -m 0644 index.html $(WEBDIR)
	$(INSTALLCMD) -m 0755 -d $(CGIDIR)
	$(INSTALLCMD) -m 0755 radiocmd.cgi $(CGIDIR)
