# 𝜫 Radio

Low resource Internet radio using Raspbian on a Pi

## Contents

The software installs following files:

  - /etc/systemd/system/radio.service
  - /usr/local/sbin/radiosrv
  - /usr/local/bin/radiocmd
  - /etc/radio/omx.keys
  - /etc/default/radio
  - /var/www/radio/index.html
  - /usr/local/lib/cgi-bin/radiocmd.cgi


## Installation

Installation is as simple as running `make install`.
It will install the daemon as a system service, which is
started in the main system startup sequence.

To make web interface work, a prerequisite is a working
HTTP server which can handle CGI scripts.  Your configuration
has to allow running CGI scripts from `/usr/local/lib/cgi-bin'
and map it to `/cgi-bin`. The default settings of Boa
web server should work fine, but you need to add this line:

    ScriptAlias /cgi-bin/ /usr/local/lib/cgi-bin/


## Usage

After installation, change file `/etc/default/radio` and set the
RADIOSTREAMADDRESS there to the address of your favorite Internet
radio station.  This address will be used by the daemon.

Then, you may start radio with following command:

    radiocmd start

and stop it with command:

    radiocmd stop

Other commands are also available.  For example, you may use the
daemon to play any stream from internet with following command:

    radiocmd url=http://your.stream.address/x/y/z



